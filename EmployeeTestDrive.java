package W4;

import java.util.Scanner;
public class EmployeeTestDrive 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		Employee E1 = new Employee();
		Employee E2 = new Employee();
		double salary;
		
		System.out.println("Nhap luong thang nhan vien thu 1: ");
		salary = sc.nextDouble();
		E1.setMSalary(salary);
		
		System.out.println("Nhap luong thang nhan vien thu 2: ");
		salary = sc.nextDouble();
		E2.setMSalary(salary);
		
		System.out.println("Luong nam nhan vien thu 1:");
		System.out.println(E1.getMSalary()* 12);
		
		System.out.println("Luong nam nhan vien thu 2:");
		System.out.println(E2.getMSalary()* 12);
		
		System.out.println("Luong nam nhan vien thu 1 sau khi tang la:");
		System.out.println(E1.getMSalary()* 12 * 1.1);
		
		System.out.println("Luong nam nhan vien thu 2 sau khi tang la:");
		System.out.println(E2.getMSalary()* 12 *1.1);
	}

}
