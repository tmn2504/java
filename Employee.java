package W4;

public class Employee 
{
	private String FirstName;
	private String LastName;
	double MSalary;
	
	public void setFirstName(String FN)
	{
		FirstName = FN;
	}
	
	public void setLastName(String LN)
	{
		LastName = LN;
	}
	
	public void setMSalary(double S)
	{
		if (S<0)
		{
			MSalary = 0.0;
		}
		MSalary = S;
	}
	
	public String getFirstName()
	{
		return FirstName;
	}
	
	public String getLastName()
	{
		return LastName;
	}
	
	public double getMSalary()
	{
		return MSalary;
	}
	
	public Employee()
	{
		FirstName = null;
		LastName = null;
		MSalary = 0.0;
	}
	
}

