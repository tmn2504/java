package tuan8.peoples;

public class Employee extends Person {
	private double salary;
	
	public Employee()
	{
		super();
		salary = 0.0;
	}
	public Employee(String name, int d, int m, int y, double salary)
	{
		super(name, d, m, y);
		this.salary = salary;
	}
	public Employee (String name, Date birthday, double salary)
	{
		super(name, birthday);
		this.salary = salary;
	}
	public double getSalary()
	{
		return salary;
	}
	public String toString()
	{
		return super.toString() +" "+ salary;
	}
}
