package tuan8.peoples;

public class Date 
{
	int day;
	int month;
	int year;
	
	public Date()
	{
		day = 1;
		month = 1;
		year = 1000;
	}
	public Date(int d, int m, int y)
	{
		day = d;
		month = m;
		year = y;
	}
	Date(Date n)
	{
		day = n.day;
		month = n.month;
		year = n.year;
	}
	public String toString()
	{
		return day + "/" + month + "/" + year;
	}
}