package tuan8.peoples;

public class Person {
	String name;
	Date birthday;
	
	public Person()
	{
		name = new String();
		birthday = new Date();
	}
	public Person(String n, int d, int m, int y)
	{
		name = new String(n);
		birthday = new Date(d,m,y);
	}
	public Person(String n, Date d)
	{
		name = new String(n);
		birthday = new Date(d);
	}
	public String getName()
	{
		return name;
	}
	public String toString()
	{
		return name + " " + birthday;
	}
}
