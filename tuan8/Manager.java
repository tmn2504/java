package tuan8;

import tuan8.peoples.Date;
import tuan8.peoples.Employee;
public class Manager extends Employee
{
	private Employee assistant;
	
	public Manager() 
	{
		super();
		assistant = new Employee();
	}
	public Manager (String name, Date birthday, double salary)
	{
		super(name, birthday,salary);
	}
	public void setAssistant(Employee temp)
	{
		assistant = temp;
	}
	public String toString()
	{
		return String.format("%s have Assistant: %s", super.toString(), assistant.getName());
	}
}
