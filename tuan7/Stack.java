package tuan5;

public class Stack {
	Node top;
	int size;
	Stack()
	{
		top = null;
		size = 0;
	}
	public boolean isEmpty()
	{
		if (size == 0)
		{
			System.out.println("Stack rong");
			return true;
		}
		else
		{	
			System.out.println("Stack khong rong");
			return false;
		}
	}
	public int numOfElement()
	{
		System.out.println("Stack co size la: " +size);
		return size;
	}
	public void push(int i)
	{
		if (size == 0)
			top = new Node(i);
		else
		{
			Node temp = new Node(i);
			temp.next = top;
			top = temp;
		}
		size++;
	}
	public int pop()
	{
        if(size > 0)
        {
            Node temp;
            temp = top;
            top = top.next;
            size--;
            return temp.item;
        }
        else
            return -1;
    }
	public int search(int a)
	{
        int index = 0;
        Node cur = top;
        while (cur != null) {
            index++;
            if(cur.item == a)
            {
                System.out.println("Index: "+index);
                return index;
            }
            cur = cur.next;
        }
        System.out.println("Khong co "+a+" trong stack");
        return 0;
    }
	public void display()
	{
        Node temp = top;
        while(temp != null)
        {
            System.out.print(temp.item + " ");
            temp = temp.next;
        }
        System.out.println("");
    }
}
